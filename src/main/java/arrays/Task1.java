package arrays;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        int arr1[] = {1, 7, 2, 56, 89, 74, 12, 6};
        int arr2[] = {21, 1, 12, 156};
        int arr3[] = createArrayOfCommonElements(arr1, arr2);
        for (int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i] + " ");
        }
        int arr4[] = createArrayWithoutDuplicates(arr1, arr2);
        for (int i = 0; i < arr4.length; i++) {
            System.out.print(arr4[i] + " ");
        }
    }

    public static int[] unionOfArrays(int[] arr1, int[] arr2) {
        int lengthOfUnionArray = arr1.length + arr2.length;
        int[] unionArray = new int[lengthOfUnionArray];
        System.arraycopy(arr1, 0, unionArray, 0, arr1.length);
        System.arraycopy(arr2, 0, unionArray, arr1.length, arr2.length);
        return unionArray;
    }

    public static int[] createArrayOfCommonElements(int arr1[], int arr2[]) {
        int newArray[];
        int indexOfElement = 0;
        if (arr1.length > arr2.length) {
            newArray = new int[arr2.length];
        } else {
            newArray = new int[arr1.length];
        }
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    newArray[indexOfElement++] = arr1[i];
                }
            }
        }
        int arrayOfUnion[] = new int[indexOfElement];
        System.arraycopy(newArray, 0, arrayOfUnion, 0, indexOfElement);
        return arrayOfUnion;
    }

    public static int[] createArrayWithoutDuplicates(int[] arr1, int[] arr2) {
        int newArray[] = unionOfArrays(arr1, arr2);
        Arrays.sort(newArray);
        if (newArray.length > 1) {
            int countOfUnique = 1;
            for (int i = 1; i < newArray.length; i++) {
                if (newArray[i] != newArray[i - 1]) {
                    countOfUnique++;
                }
            }
            int[] arrayWithoutDuplicates = new int[countOfUnique];
            int index = 0;
            if (arrayWithoutDuplicates.length > 0) {
                arrayWithoutDuplicates[index++] = newArray[0];
            }
            for (int i = 1; i < newArray.length; i++) {
                if (newArray[i] != newArray[i - 1]) {
                    arrayWithoutDuplicates[index++] = newArray[i];
                }
            }
            return arrayWithoutDuplicates;
        } else {
            System.out.println();
            return newArray;
        }
    }
}