package container;

public class ContainerString {
    private String[] arr;
    private int size;
    private static final int DEFAULT_SIZE = 16;

    public ContainerString() {
        arr = new String[DEFAULT_SIZE];
        size = 0;
    }

    private void checkSize() {
        if (arr.length == size) {
            String[] arrNew = new String[(arr.length * 3 + 1) / 2];
            for (int j = 0; j < arr.length; j++) {
                arrNew[j] = arr[j];
            }
            arr = arrNew;
        }
    }

    public boolean add(String element) {
        checkSize();
        arr[size++] = element;
        return true;
    }

    public String get(int index) {
        if (index < 0 || index > arr.length) {
            throw new IndexOutOfBoundsException("There is no such index");
        }
        for (int i = 0; i < arr.length; i++) {
            if (index == i) return arr[i];
        }
        return arr[index];
    }
}
