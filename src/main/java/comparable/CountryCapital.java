package comparable;

public class CountryCapital implements Comparable<CountryCapital> {
    private String country;
    private String capital;

    public CountryCapital(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public int compareTo(CountryCapital o) {
        return country.compareTo(o.getCountry());
    }

    @Override
    public String toString() {
        return "CountryCapital{" +
                "country='" + country + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }
}
