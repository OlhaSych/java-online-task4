package comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<CountryCapital> pairs = new ArrayList<CountryCapital>();
        pairs.add(new CountryCapital("Austria", "Vienna"));
        pairs.add(new CountryCapital("Belarus", "Minsk"));
        pairs.add(new CountryCapital("Cuba", "Havana"));
        pairs.add(new CountryCapital("Egypt", "Cairo"));
        pairs.add(new CountryCapital("Slovakia", "Bratislava"));
        pairs.add(new CountryCapital("Ukraine", "Kiev"));
        List<CountryCapital> arrayList = new ArrayList<CountryCapital>();
        CountryCapital[] array = new CountryCapital[pairs.size()];
        for (int i = 0; i < pairs.size(); i++) {
            int rand = (int) (Math.random() * pairs.size());
            array[i] = pairs.get(rand);
        }
        System.out.println();
        for (int j = 0; j < pairs.size(); j++) {
            int rand = (int) (Math.random() * pairs.size());
            arrayList.add(j, new CountryCapital(pairs.get(rand).getCountry(), pairs.get(rand).getCapital()));
        }
        Collections.sort(arrayList, new ComparatorByCapital());
        for (int j = 0; j < arrayList.size(); j++) {
            System.out.println(arrayList.get(j) + ",");
        }
    }
}
