package comparable;

import java.util.Comparator;

public class ComparatorByCapital implements Comparator<CountryCapital> {
    public int compare(CountryCapital o1, CountryCapital o2) {
        return o2.getCapital().compareTo(o1.getCapital());
    }
}
